package com.basiclab.iot.core.service.message.factory;

import com.basiclab.iot.core.service.message.AbstractNetProtoBufUdpMessage;

/**
 * Created by jiangwenping on 17/2/22.
 */
public interface IUdpMessageFactory {
    public AbstractNetProtoBufUdpMessage createCommonErrorResponseMessage(int serial, int state);
    public AbstractNetProtoBufUdpMessage createCommonResponseMessage(int serial);
}
