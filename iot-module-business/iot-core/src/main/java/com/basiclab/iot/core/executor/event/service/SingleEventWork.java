package com.basiclab.iot.core.executor.event.service;

import com.basiclab.iot.core.executor.common.utils.Loggers;
import com.basiclab.iot.core.executor.event.EventBus;
import com.basiclab.iot.core.executor.event.SingleEvent;
import com.basiclab.iot.core.thread.worker.AbstractWork;

/**
 * Created by jwp on 2017/5/5.
 * 单事件worker
 */
public class SingleEventWork extends AbstractWork{

    private final EventBus eventBus;
    private final SingleEvent singleEvent;

    public SingleEventWork(EventBus eventBus, SingleEvent singleEvent) {
        this.eventBus = eventBus;
        this.singleEvent = singleEvent;
    }

    @Override
    public void run() {
        try {
            eventBus.handleSingleEvent(singleEvent);
        }catch (Exception e){
            Loggers.gameExecutorError.error(e.toString(), e);
        }

    }
}
