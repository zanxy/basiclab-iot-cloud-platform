//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.basiclab.iot.core.expression.impl;


import com.basiclab.iot.core.expression.Expression;

public class StaticExpression implements Expression {
    private static final long serialVersionUID = -6060431150586251801L;
    private int value;

    public StaticExpression(int value) {
        this.value = value;
    }

    @Override
    public long getValue(long key) {
        return (long)this.value;
    }
}
