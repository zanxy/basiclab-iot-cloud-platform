//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.basiclab.iot.core.expression.impl;

public class MultipleExpression extends BinaryOperationExpression {
    private static final long serialVersionUID = -5663414391707378304L;

    public MultipleExpression() {
    }

    @Override
    public long getValue(long key) {
        return this.left.getValue(key) * this.right.getValue(key);
    }

    @Override
    public int getPriority() {
        return 2;
    }
}
