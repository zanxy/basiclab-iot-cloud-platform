//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.basiclab.iot.core.expression.impl;

public class SubtractExpression extends BinaryOperationExpression {
    private static final long serialVersionUID = 1026380096191594448L;

    public SubtractExpression() {
    }

    @Override
    public long getValue(long key) {
        return this.left.getValue(key) - this.right.getValue(key);
    }

    @Override
    public int getPriority() {
        return 1;
    }
}
