package com.basiclab.iot.core.service.rpc.service.server;

import com.basic.common.network.annotation.RpcServiceAnnotation;
import com.basic.common.network.annotation.RpcServiceBoEnum;
import com.basic.common.network.enums.BOEnum;
import com.basiclab.iot.core.service.rpc.service.client.HelloService;
import org.springframework.stereotype.Repository;

/**
 * Created by jwp on 2017/3/7.
 */
@RpcServiceAnnotation(HelloService.class)
@RpcServiceBoEnum(bo = BOEnum.WORLD)
@Repository
public class HelloServiceImpl implements HelloService {

    @Override
    public String hello(String name) {
        return "Hello! " + name;
    }
}

