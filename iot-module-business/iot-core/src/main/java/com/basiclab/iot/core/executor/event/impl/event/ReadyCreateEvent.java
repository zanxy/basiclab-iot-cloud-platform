package com.basiclab.iot.core.executor.event.impl.event;

import com.basiclab.iot.core.executor.event.CycleEvent;
import com.basiclab.iot.core.executor.event.EventParam;
import com.basiclab.iot.core.executor.event.EventType;

/**
 * Created by jiangwenping on 17/1/16.
 * updateService使用
 */
public class ReadyCreateEvent extends CycleEvent {

    public ReadyCreateEvent(EventType eventType, long eventId, EventParam... parms){
        super(eventType, eventId, parms);
    }

    @Override
    public void call() {

    }
}
