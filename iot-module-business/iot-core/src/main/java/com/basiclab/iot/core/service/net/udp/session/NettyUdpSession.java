package com.basiclab.iot.core.service.net.udp.session;

import com.basiclab.iot.core.service.message.AbstractNetMessage;
import com.basiclab.iot.core.service.net.tcp.session.NettySession;
import io.netty.channel.Channel;

/**
 * Created by jwp on 2017/2/16.
 * netty的udp session
 */
public class NettyUdpSession  extends NettySession {

    private final NettyUdpNetMessageSender nettyUdpNetMessageSender;
    public NettyUdpSession(Channel channel) {
        super(channel);
        this.nettyUdpNetMessageSender = new NettyUdpNetMessageSender(this);
    }

    public void write(AbstractNetMessage msg) throws Exception {
        if (msg != null) {
            channel.writeAndFlush(msg).sync();
        }
    }
}
