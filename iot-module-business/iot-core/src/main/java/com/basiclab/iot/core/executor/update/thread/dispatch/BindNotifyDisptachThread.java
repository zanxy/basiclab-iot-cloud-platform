package com.basiclab.iot.core.executor.update.thread.dispatch;

import com.basiclab.iot.core.executor.event.EventBus;
import com.basiclab.iot.core.executor.update.pool.UpdateBindExecutorService;

/**
 * Created by jwp on 2017/2/23.
 */
public class BindNotifyDisptachThread extends LockSupportDisptachThread{

    public BindNotifyDisptachThread(EventBus eventBus, UpdateBindExecutorService updateBindExcutorService
            , int cycleSleepTime, long minCycleTime) {
        super(eventBus, updateBindExcutorService, cycleSleepTime, minCycleTime);
    }
}
