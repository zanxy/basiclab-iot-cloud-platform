package com.basiclab.iot.core.executor.event;


import com.basiclab.iot.core.executor.event.common.IEvent;

import java.io.Serializable;

/**
 * Created by jiangwenping on 17/1/9.
 */
public abstract class AbstractEvent<ID extends Serializable> implements IEvent {

    private EventType eventType;
    private EventParam[] eventParamps;
    private ID id;

    @Override
    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    @Override
    public EventType getEventType() {
        return this.eventType;
    }

    @Override
    public EventParam[] getParams() {
        return this.eventParamps;
    }

    @Override
    public void setParams(EventParam... eventParams) {
        this.eventParamps = eventParams;
    }

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }
}
