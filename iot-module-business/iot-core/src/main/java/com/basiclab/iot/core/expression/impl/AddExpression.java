//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.basiclab.iot.core.expression.impl;


import com.basiclab.iot.core.expression.Expression;

public class AddExpression extends BinaryOperationExpression {
    private static final long serialVersionUID = -5375067912237316734L;

    public AddExpression() {
    }

    public AddExpression(Expression left) {
        this.left = left;
    }

    public long getValue(long key) {
        return this.left.getValue(key) + this.right.getValue(key);
    }

    @Override
    public int getPriority() {
        return 1;
    }
}
