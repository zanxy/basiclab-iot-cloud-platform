//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.basiclab.iot.core.timer;

import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

public class TimeSlot<E> {
    private int id;
    private Set<E> elements = new ConcurrentSkipListSet();

    public TimeSlot(int id) {
        this.id = id;
    }

    public void add(E e) {
        this.elements.add(e);
    }

    public boolean remove(E e) {
        return this.elements.remove(e);
    }

    public Set<E> getElements() {
        return this.elements;
    }

    @Override
    public int hashCode() {
        boolean prime = true;
        int result = 1;
        result = 31 * result + this.id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (this.getClass() != obj.getClass()) {
            return false;
        } else {
            TimeSlot timeSlot = (TimeSlot)obj;
            return timeSlot.id == this.id;
        }
    }
}
