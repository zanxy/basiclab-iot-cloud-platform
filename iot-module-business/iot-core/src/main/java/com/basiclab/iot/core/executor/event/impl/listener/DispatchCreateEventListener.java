package com.basiclab.iot.core.executor.event.impl.listener;

import com.basiclab.iot.core.executor.common.utils.Constants;
import com.basiclab.iot.core.executor.event.EventParam;
import com.basiclab.iot.core.executor.event.common.IEvent;
import com.basiclab.iot.core.executor.event.impl.event.FinishEvent;
import com.basiclab.iot.core.executor.event.impl.event.UpdateEvent;
import com.basiclab.iot.core.executor.update.cache.UpdateEventCacheService;
import com.basiclab.iot.core.executor.update.entity.IUpdate;
import com.basiclab.iot.core.executor.update.service.UpdateService;
import com.basiclab.iot.core.executor.update.thread.dispatch.DispatchThread;

/**
 * Created by jiangwenping on 17/1/11.
 */
public class DispatchCreateEventListener extends CreateEventListener {

    private final DispatchThread dispatchThread;

    private final UpdateService updateService;
    public DispatchCreateEventListener(DispatchThread dispatchThread, UpdateService updateService) {
        super();
        this.dispatchThread = dispatchThread;
        this.updateService = updateService;
    }

    @Override
    public void fireEvent(IEvent event) {
        super.fireEvent(event);
        EventParam[] eventParams = event.getParams();
        IUpdate iUpdate = (IUpdate) eventParams[0].getT();
        if(iUpdate.isActive()) {
//            UpdateEvent updateEvent = new UpdateEvent(Constants.EventTypeConstans.updateEventType, iUpdate.getUpdateId(), event.getParams());
            UpdateEvent updateEvent = UpdateEventCacheService.createUpdateEvent();
            updateEvent.setEventType(Constants.EventTypeConstans.updateEventType);
            updateEvent.setId(iUpdate.getUpdateId());
            updateEvent.setParams(eventParams);
            updateEvent.setInitFlag(true);
            updateEvent.setUpdateAliveFlag(true);
            this.dispatchThread.addUpdateEvent(updateEvent);
        }else{
            FinishEvent finishEvent = new FinishEvent(Constants.EventTypeConstans.finishEventType, iUpdate.getUpdateId(), eventParams);
            dispatchThread.addFinishEvent(finishEvent);
        }
    }
}
