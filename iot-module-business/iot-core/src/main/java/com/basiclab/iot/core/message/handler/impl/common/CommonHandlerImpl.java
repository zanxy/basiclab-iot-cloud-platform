package com.basiclab.iot.core.message.handler.impl.common;

import com.basiclab.iot.core.bootstrap.manager.LocalMananger;
import com.basic.common.network.annotation.MessageCommandAnnotation;
import com.basiclab.iot.core.message.handler.AbstractMessageHandler;
import com.basiclab.iot.core.message.logic.tcp.online.client.OnlineHeartClientTcpMessage;
import com.basiclab.iot.core.service.message.AbstractNetMessage;
import com.basiclab.iot.core.service.message.command.MessageCommandIndex;
import com.basiclab.iot.core.service.message.factory.TcpMessageFactory;

/**
 * Created by jiangwenping on 17/2/15.
 */
public class CommonHandlerImpl extends AbstractMessageHandler {

    @MessageCommandAnnotation(command = MessageCommandIndex.ONLINE_HEART_CLIENT_TCP_MESSAGE)
    public AbstractNetMessage handleOnlineHeartMessage(OnlineHeartClientTcpMessage message) throws Exception {
        TcpMessageFactory messageFactory = LocalMananger.getInstance().getLocalSpringBeanManager().getTcpMessageFactory();
        return messageFactory.createCommonResponseMessage(message.getSerial());
    }
}
