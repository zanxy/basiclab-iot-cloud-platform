package com.basiclab.iot.core.service.lookup;

/**
 * Created by jiangwenping on 17/2/21.
 *
 */
public interface ILongId {
    public long longId();
}
