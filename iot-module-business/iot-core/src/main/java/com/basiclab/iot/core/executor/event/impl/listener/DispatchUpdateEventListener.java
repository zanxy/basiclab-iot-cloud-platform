package com.basiclab.iot.core.executor.event.impl.listener;

import com.basiclab.iot.core.executor.common.utils.Constants;
import com.basiclab.iot.core.executor.event.CycleEvent;
import com.basiclab.iot.core.executor.event.EventParam;
import com.basiclab.iot.core.executor.event.common.IEvent;
import com.basiclab.iot.core.executor.event.impl.event.FinishEvent;
import com.basiclab.iot.core.executor.event.impl.event.UpdateEvent;
import com.basiclab.iot.core.executor.update.cache.UpdateEventCacheService;
import com.basiclab.iot.core.executor.update.entity.IUpdate;
import com.basiclab.iot.core.executor.update.pool.IUpdateExecutor;
import com.basiclab.iot.core.executor.update.service.UpdateService;
import com.basiclab.iot.core.executor.update.thread.dispatch.DispatchThread;

/**
 * Created by jiangwenping on 17/1/11.
 */
public class DispatchUpdateEventListener extends UpdateEventListener {
    private final DispatchThread dispatchThread;
    private final UpdateService updateService;
    public DispatchUpdateEventListener(DispatchThread dispatchThread, UpdateService updateService) {
        this.dispatchThread = dispatchThread;
        this.updateService = updateService;
    }


    @Override
    public void fireEvent(IEvent event) {
//        if(Loggers.gameExecutorUtil.isDebugEnabled()){
//            Loggers.gameExecutorUtil.debug("处理update");
//        }
        super.fireEvent(event);

        //提交执行线程
        CycleEvent cycleEvent = (CycleEvent) event;
        EventParam[] eventParams = event.getParams();
        for(EventParam eventParam: eventParams) {
            IUpdate iUpdate = (IUpdate) eventParam.getT();
            boolean aliveFlag = cycleEvent.isUpdateAliveFlag();
            if (aliveFlag) {
                IUpdateExecutor iUpdateExecutor = dispatchThread.getiUpdateExecutor();
                iUpdateExecutor.executorUpdate(dispatchThread, iUpdate, cycleEvent.isInitFlag(), cycleEvent.getUpdateExcutorIndex());
            } else {
                FinishEvent finishEvent = new FinishEvent(Constants.EventTypeConstans.finishEventType, iUpdate.getUpdateId(), eventParams);
                dispatchThread.addFinishEvent(finishEvent);
            }
        }

        //如果是update，需要释放cache
        if(cycleEvent instanceof UpdateEvent){
            UpdateEvent updateEvent = (UpdateEvent) cycleEvent;
            UpdateEventCacheService.releaseUpdateEvent(updateEvent);
        }
    }
}
