package com.basiclab.iot.core.service.message.facade;

import com.basic.common.network.exception.GameHandlerException;
import com.basiclab.iot.core.service.message.AbstractNetMessage;

/**
 * Created by jiangwenping on 17/2/8.
 */
public interface IFacade {
    public AbstractNetMessage dispatch(AbstractNetMessage message)  throws GameHandlerException;
}
