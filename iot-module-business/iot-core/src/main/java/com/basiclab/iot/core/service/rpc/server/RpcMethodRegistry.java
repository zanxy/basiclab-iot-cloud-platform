package com.basiclab.iot.core.service.rpc.server;

import com.basiclab.iot.core.bootstrap.manager.LocalMananger;
import com.basic.common.network.annotation.RpcServiceAnnotation;
import com.basic.common.network.constant.GlobalConstants;
import com.basic.common.network.constant.Loggers;
import com.basic.common.network.constant.ServiceName;
import com.basic.common.network.scanner.ClassScanner;
import com.basiclab.iot.core.service.IService;
import com.basiclab.iot.core.service.Reloadable;
import com.basiclab.iot.core.service.config.GameServerConfigService;
import com.basiclab.iot.core.service.rpc.serialize.protostuff.ProtostuffSerializeI;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by jwp on 2017/3/7.
 */
@Service
public class RpcMethodRegistry implements Reloadable, IService {

    public static Logger logger = Loggers.serverLogger;

    public ClassScanner classScanner = new ClassScanner();

    private final ConcurrentHashMap<String, Object> registryMap = new ConcurrentHashMap<String, Object>();

    @Override
    public String getId() {
        return ServiceName.RpcMethodRegistry;
    }

    @Override
    public void startup() throws Exception {
        reload();
    }

    @Override
    public void shutdown() throws Exception {

    }

    @Override
    public void reload() throws Exception {
        GameServerConfigService gameServerConfigService = LocalMananger.getInstance().getLocalSpringServiceManager().getGameServerConfigService();
        String packageName = gameServerConfigService.getGameServerConfig().getRpcServicePackage();
        loadPackage(packageName,
                GlobalConstants.FileExtendConstants.Ext);
    }

    public void loadPackage(String namespace, String ext) throws Exception {
        String[] fileNames = classScanner.scannerPackage(namespace, ext);
        // 加载class,获取协议命令
        if(fileNames != null) {
            for (String fileName : fileNames) {
                String realClass = namespace
                                   + '.'
                                   + fileName.subSequence(0, fileName.length()
                        - (ext.length()));
                Class<?> messageClass = Class.forName(realClass);

                logger.info("rpc load:" + messageClass);
                RpcServiceAnnotation rpcServiceAnnotation = messageClass.getAnnotation(RpcServiceAnnotation.class);
                if(rpcServiceAnnotation != null) {
                    String interfaceName = messageClass.getAnnotation(RpcServiceAnnotation.class).value().getName();
                    ProtostuffSerializeI rpcSerialize = LocalMananger.getInstance().getLocalSpringBeanManager().getProtostuffSerialize();
                    Object serviceBean = rpcSerialize.newInstance(messageClass);
                    registryMap.put(interfaceName, serviceBean);
                    logger.info("rpc register:" + messageClass);
                }
            }
        }
    }

    public Object getServiceBean(String className){
        return registryMap.get(className);
    }

}
