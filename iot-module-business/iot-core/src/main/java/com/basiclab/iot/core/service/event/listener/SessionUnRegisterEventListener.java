package com.basiclab.iot.core.service.event.listener;

import com.basic.common.network.annotation.GlobalEventListenerAnnotation;
import com.basiclab.iot.core.executor.event.AbstractEventListener;
import com.basiclab.iot.core.service.event.SingleEventConstants;

/**
 * Created by jiangwenping on 2017/5/22.
 */
@GlobalEventListenerAnnotation
public class SessionUnRegisterEventListener extends AbstractEventListener {
    @Override
    public void initEventType() {
        register(SingleEventConstants.sessionUnRegister);
    }
}
