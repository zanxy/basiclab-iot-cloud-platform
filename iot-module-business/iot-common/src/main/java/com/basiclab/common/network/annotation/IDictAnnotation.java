package com.basiclab.common.network.annotation;

import java.lang.annotation.*;

/**
 * Created by jiangwenping on 17/5/9.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface IDictAnnotation {

}
