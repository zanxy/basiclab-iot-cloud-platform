# 基础架构实验室-物联网平台
<div align=center>
    <img src="https://img-blog.csdnimg.cn/59df0559ee9b498dac941cd15a9cbd81.png" width = 30% height = 30% />
</div>

![basiclab](https://visitor-badge.glitch.me/badge?page_id=basiclab-iot-cloud-platform)
![Codacy Badge](https://api.codacy.com/project/badge/Grade/e8d527d692c24633aba4f869c1c5d6ad)
![Codacy Badge](https://api.codacy.com/project/badge/Grade/e8d527d692c24633aba4f869c1c5d6ad)
![Codacy Badge](https://api.codacy.com/project/badge/Grade/e8d527d692c24633aba4f869c1c5d6ad)
![Codacy Badge](https://api.codacy.com/project/badge/Grade/e8d527d692c24633aba4f869c1c5d6ad)
![Codacy Badge](https://api.codacy.com/project/badge/Grade/e8d527d692c24633aba4f869c1c5d6ad)
![Codacy Badge](https://api.codacy.com/project/badge/Grade/e8d527d692c24633aba4f869c1c5d6ad)
![basiclab](https://visitor-badge.glitch.me/badge?page_id=basiclab-iot-cloud-platform)


## 🦅 架构图
<div align=center>
    <img src="https://gitee.com/vegetable-chicken-blog/blog-images/raw/master/%E7%89%A9%E8%81%94%E7%BD%91%E5%B9%B3%E5%8F%B0%E6%9E%B6%E6%9E%84.jpg" />
</div>

## 🐯 平台简介

**BasicLab基础架构实验室**，目前只有互联网大厂才有基础架构团队，基础架构的目的是为了降本增效，用更少的时间开发更多的项目，大大降低企业成本
**BasicLab基础架构实验室**，打造中国第一流的基础架构中间件平台。

开源不易，给项目点点 Star 吧，真的对我们很重要！！！

basiclab-iot-cloud-platform 基于Java8,SpringBoot 2.x,SpringCLoud,Netty,Nacos,PostgreSQL,TDengine,Redis,Seata,Grafana,Jenkins,Kubernetes等开发, 
是一个开箱即用,可二次开发的企业级物联网基础平台。平台实现了物联网相关的众多基础功能,能帮助你快速建立物联网相关业务系统。

## 🐉 核心特性

支持统一物模型管理,多种设备,多种厂家,统一管理。

统一对象接入服务,多协议适配网关(TCP,MQTT,UDP,CoAP,HTTP等),屏蔽网络编程复杂性,灵活接入不同厂家不同协议的设备。

统一安全认证服务,支持不同设备的安全认证，大大提升安全等级，覆盖终端安全、传输安全、云端安全

灵活的规则引擎,设备告警,消息通知,数据转发.

强大的ReactorQL引擎,使用SQL来处理实时数据.

## 🐻 物联网设备接入流程
<div align=center>
    <img src="https://gitee.com/vegetable-chicken-blog/blog-images/raw/master/%E7%89%A9%E8%81%94%E7%BD%91%E5%B9%B3%E5%8F%B0%E8%AE%BE%E5%A4%87%E6%8E%A5%E5%85%A5%E6%B5%81%E7%A8%8B.jpg" />
</div>

## 😎 开源协议

**为什么推荐使用本项目？**

① 本项目采用比 Apache 2.0 更宽松的 [MIT License](https://gitee.com/zhijiantianya/ruoyi-vue-pro/blob/master/LICENSE) 开源协议，社区版可 100% 免费使用，不用保留类作者、Copyright 信息。

## 🐇 技术栈

1. [Spring Boot 2.3.x](https://spring.io/projects/spring-boot)
2. [Spring Cloud 2.2.x](https://spring.io/projects/spring-cloud)
3. [R2DBC](https://r2dbc.io/) 响应式关系型数据库驱动
4. [Project Reactor](https://projectreactor.io/) 响应式编程框架
4. [Netty](https://netty.io/) 高性能网络编程框架
5. [ElasticSearch](https://www.elastic.co/cn/products/enterprise-search) 全文检索，日志，时序数据存储
6. [PostgreSQL](https://www.postgresql.org) 强大的关系型数据库

### 🦁 物联网平台功能

|     | 功能    | 描述                              |
|-----|-------|---------------------------------|
|  🚀   | 设备接入网关  | 设备接入唯一入口          |
|  🚀   | 网络组件  | 可以启动TCP,MQTT,UDP,CoAP,HTTP服务端/客户端属性接收设备集群消息           |
|  🚀   | 规则引擎  | 强大的ReactorQL引擎,使用SQL来处理实时数据      |
|  🚀   | 协议管理  | TCP,MQTT,UDP,CoAP,HTTP,自定义协议    |
| 🚀    | 物模型管理  | 根据不同的产品适配不同的通信协议  |
|  🚀   | 设备影子  | 设备影子维护设备的实时属性状态                    |
| 🚀  | 设备管理  | 设备管理       |
| 🚀  | 消息中心  | 消息中心       |
| 🚀    | 产品管理  | 产品管理         |
| 🚀    | 证书管理  | 证书管理         |
| 🚀    | 虚拟测试  | 虚拟测试         |
| 🚀  | 日志管理  | 日志管理 |
| 🚀  | 文件管理  | 文件管理       |
| 🚀  | 物联管理   | 物联管理         |
| 🚀  | 数据中心  | 数据中心 |
| 🚀  | 通知管理  | 通知管理             |
| 🚀  | 安全认证服务 | 安全认证服务     |
|  🚀   | OTA升级  | OTA升级                 |

### 🕊 统一认证平台功能

|     | 功能    | 描述                              |
|-----|-------|---------------------------------|
|  🚀   | 用户管理  | 用户管理          |
|  🚀   | 权限管理  | 配置系统菜单、操作权限、按钮权限标识等，本地缓存提供性能          |
|  🚀   | 菜单管理  | 角色菜单权限分配、设置角色按机构进行数据范围权限划分      |
|  🚀   | 部门管理  |  配置系统组织机构（公司、部门、小组），树结构展现支持数据权限   |
| 🚀    | 应用管理  | 配置不同应用的token、rsa鉴权等信息|
|  🚀   | 租户管理  | 配置系统用户所在租户相关的属性                    |

## 🐱 统一网关API平台
|     | 功能    | 描述                              |
|-----|-------|---------------------------------|
|  🚀   | 路由规则  | 用户是系统操作者，该功能主要完成系统用户配置          |
|  🚀   | 熔断策略  | 当前系统中活跃用户状态监控，支持手动踢下线           |
|  🚀   | 服务降级  | 角色菜单权限分配、设置角色按机构进行数据范围权限划分      |
|  🚀   | 线程池隔离  | 配置系统菜单、操作权限、按钮权限标识等，本地缓存提供性能    |
| 🚀    | 服务限流  | 配置系统组织机构（公司、部门、小组），树结构展现支持数据权限  |
|  🚀   | 应用签名  | 配置系统用户所属担任职务                    |

## 🐘 统一网关
|     | 功能    | 描述                              |
|-----|-------|---------------------------------|
|  🚀   | 路由流量风控  | 用户是系统操作者，该功能主要完成系统用户配置          |
|  🚀   | 熔断流量风控  | 当前系统中活跃用户状态监控，支持手动踢下线           |
|  🚀   | 服务降级流量风控  | 角色菜单权限分配、设置角色按机构进行数据范围权限划分      |
|  🚀   | 线程池隔离流量风控  | 配置系统菜单、操作权限、按钮权限标识等，本地缓存提供性能    |
| 🚀    | 服务限流流量风控  | 配置系统组织机构（公司、部门、小组），树结构展现支持数据权限  |
|  🚀   | 应用授权/校验  | 配置系统用户所属担任职务                    |

## 🤝 企业版本
本开源版本为社区版本，如果需要企业版本请联系我们，下方有联系方式

## 🦊 基础架构整体规划

| 项目名                  | 说明                     | 传送门                                                                                                                                 |
|----------------------|------------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| `basiclab-unified-code-quality-audit-platform`      | 统一代码质量审计平台        | **[Gitee](https://gitee.com/vegetable-chicken-blog/basiclab-unified-code-quality-audit-platform)**     |
| `basiclab-unified-api-gateway-platform`        | 统一API网关平台       | **[Gitee](https://gitee.com/vegetable-chicken-blog/basiclab-unified-api-gateway-platform)** |
| `basiclab-unified-identity-authentication-platform`   | 统一身份认证平台 | **[Gitee](https://gitee.com/vegetable-chicken-blog/basiclab-unified-identity-authentication-platform)** |
| `basiclab-unified-bpm-platform` | 统一工单流程管理平台        | **[Gitee](https://gitee.com/vegetable-chicken-blog/basiclab-unified-bpm-platform)**  |                                                              
| `basiclab-unified-publish-platform` | 统一发布平台       | **[Gitee](https://gitee.com/vegetable-chicken-blog/basiclab-unified-publish-platform)** |                                                                
| `basiclab-things-link-cloud-platform` | 物联网云平台        | **[Gitee](https://gitee.com/vegetable-chicken-blog/basiclab-things-link-cloud-platform)**  |                                                              
| `basiclab-bigdata-bi-visualization-platform` | 大数据BI可视化平台        | **[Gitee](https://gitee.com/vegetable-chicken-blog/basiclab-bigdata-bi-visualization-platform)**  |                                                              
| `basiclab-bigdata-data-quality-control-platform` | 大数据数据质量管控平台  | **[Gitee](https://gitee.com/vegetable-chicken-blog/basiclab-bigdata-data-quality-control-platform)** |                                                               
| `basiclab-bigdata-stream-application-development-platform` | 大数据流应用开发平台        | **[Gitee](https://gitee.com/vegetable-chicken-blog/basiclab-bigdata-stream-application-development-platform)** |                                                     

## 👇 交流社区（添加作者WX: BasicLab888 邀请进微信群，备注：basiclab）
<div align=center>
    <img src="https://gitee.com/vegetable-chicken-blog/blog-images/raw/master/%E8%81%94%E7%B3%BB%E6%96%B9%E5%BC%8F.jpg" width = 70% height = 70% />
</div>

## 👇 感谢您的支持，我们会继续努力
<div align=center>
    <img src="https://gitee.com/vegetable-chicken-blog/blog-images/raw/master/%E6%94%B6%E6%AC%BE%E7%A0%81.jpg" width = 70% height = 70% />
</div>

## 😊 版权使用说明
BasicLab基础架构实验室开源平台遵循 [MIT](License) 协议。 允许商业使用。